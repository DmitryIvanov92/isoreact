if unix
in package.json
----------------
 "scripts": {
    "build": "NODE_ENV='production' webpack -p",
    "webpack-devserver": "webpack-dev-server --debug --hot --devtool eval-source-map --output-pathinfo --watch --colors --inline --content-base public --port 8050 --host 0.0.0.0"
  }
---------------
if windows 
--------------------
"scripts": {
    "build": "set NODE_ENV='production' && webpack -p",
    "webpack-devserver": "webpack-dev-server --debug --hot --devtool eval-source-map --output-pathinfo --watch --colors --inline --content-base public --port 8050 --host 0.0.0.0"
  }
--------------------
