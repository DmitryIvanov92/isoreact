import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router';
import { Grid, Nav, Navbar, NavItem } from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';
import './bootstrap.css';

const propTypes = {
  children: PropTypes.node.isRequired,
};

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      brand: 'Hello World',
    };
  }
  render() {
    return (
      <div>
        <Navbar>
          <Navbar.Header>
            <Navbar.Brand>
              <Link to="/">{this.state.brand}</Link>
            </Navbar.Brand>
            <Navbar.Toggle />
          </Navbar.Header>
          <Navbar.Collapse>
            <Nav navbar>
              <LinkContainer to="/time">
                <NavItem>Time</NavItem>
              </LinkContainer>
              <LinkContainer to="/counters">
                <NavItem>Counters</NavItem>
              </LinkContainer>
            </Nav>
          </Navbar.Collapse>
        </Navbar>
        <Grid>
          {this.props.children}
        </Grid>
      </div>
    );
  }
}

App.propTypes = propTypes;

export default App;
