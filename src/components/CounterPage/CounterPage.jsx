import React, { Component } from 'react';
import { PageHeader } from 'react-bootstrap';
import StateCounter from './StateCounter';
import ReduxCounter from './ReduxCounter';

class CounterPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: 'Counters',
    };
  }
  render() {
    return (
      <div>
        <PageHeader>Counters</PageHeader>
        <h3>State Counter</h3>
        <StateCounter />
        <h3>Redux Counter</h3>
        <ReduxCounter />
      </div>
    );
  }
}

export default CounterPage;
