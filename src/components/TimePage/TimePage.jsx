import React, { Component } from 'react';

class TimePage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: 'Time',
    };
  }
  render() {
    return <div>{this.state.name}</div>;
  }
}

export default TimePage;
