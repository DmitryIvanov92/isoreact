import express from 'express';
import React from 'react';
import ReactDom from 'react-dom/server';
import { match, RouterContext } from 'react-router';
import { Provider } from 'react-redux';
import routes from './routes';
import configureStore from './redux/configureStore';

const app = express();
const assetUrl = process.env.NODE_ENV !== 'production' ? 'http://localhost:8050' : '/';
const PORT = process.env.PORT || 3001;

function renderHTML(componentHTML) {
  return `
    <!DOCTYPE html>
    <html lang="en">
      <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>Hello React</title>
        <link rel="stylesheet" href="${assetUrl}/public/assets/styles.css" />
      </head>
      <body>
        <div id="react-view">${componentHTML}</div>
        <script src="${assetUrl}/public/assets/bundle.js" type="application/javascript"></script>
      </body>
    </html>
  `;
}
/* eslint-disable */
app.use((req, res) => {
  match({ routes, location: req.url }, (error, redirectLocation, renderProps) => {
    if (redirectLocation) {
      return res.redirect(301, redirectLocation.pathname + redirectLocation.search);
    }

    if (error) {
      return res.status(500).send(error.message);
    }

    if (!renderProps) {
      return res.status(404).send('Not found');
    }

    const store = configureStore(
      typeof window !== 'undefined' && window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
    );

    const componentHTML = ReactDom.renderToString(
      <Provider store={store}>
        <RouterContext {...renderProps} />
      </Provider>
    );

    res.end(renderHTML(componentHTML));
  });
});
/* eslint-enable */

app.listen(PORT, () => {
  console.log(`Server listening on ${PORT}`);
});
